# Composer template for Drupal projects

[![Build Status](https://travis-ci.org/drupal-composer/drupal-project.svg?branch=8.x)](https://travis-ci.org/drupal-composer/drupal-project)

This project is based on the [drupal/recommended-project](https://github.com/drupal/recommended-project) and [pfrenssen/drupal-project](https://github.com/pfrenssen/drupal-project).
It is a composer starter-kit and uses [Phing](https://www.phing.info/) to generate some scaffold files that speeds up
setup and development.
Please, refer to [composer.json](./composer.json) for the installed version of Drupal. Currently is set to `^9`.

## Additional features

This fork has the following additions to the original version of drupal-project:

* **Customizable builds**: Different builds for production and development
  environments are created with [Phing](https://www.phing.info/). These can be
  fully customized using a set of configuration files.
* **Preconfigured test suites**: PHPUnit is being set up for running unit tests
  as well as kernel tests, web tests and javascript tests.
* **PHP CodeSniffer**: Check compliance with coding standards with a single
  command, or set it up to scan automatically whenever you `git push`.
* **Travis CI**: Integrates seamlessly with [Travis CI](https://travis-ci.com/)
  so you can automate your tests right from the start. No setup needed!

## Upcoming features

Behat is going to be supported soon.

## Usage

First you need to [install composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx).

> Note: The repository includes a version of composer in its dependencies. This is intended so that all systems run the
> same version. However, composer should be available globally to initially install the dependencies. Prefer to use the
> repository's composer.

After that you can create the project:

```
git clone https://github.com/idimopoulos/drupal-project.git some-dir
cd some-dir
composer install
```

You can refer also to [Composer documentation](https://getcomposer.org/doc/) on how to manage your dependencies.

## What does the template do?

Composer performs the following tasks in conjunction with the drupal-recommended project:

* Installs Drupal in the `web`-directory.
* Modules (packages of type `drupal-module`) will be placed in `web/modules/contrib/`
* Theme (packages of type `drupal-theme`) will be placed in `web/themes/contrib/`
* Profiles (packages of type `drupal-profile`) will be placed in `web/profiles/contrib/`
* Creates default writable versions of `settings.php` and `services.yml`.
* Creates `web/sites/default/files`-directory.
* Creates environment variables based on your .env file. See [.env.example](.env.example).

## Create Phing configuration files.
Drupal recommended project comes with the [build.properties.dist] file that includes demo values for the project. When
forking the project, copy the file over to `build.properties` in the same folder and override all values that are
different in the new project.

This `build.properties` file should not contain passwords or other sensitive information. Each individual developer can
then create their own `build.properties.local` with all the variables they want to override. Phing includes these three
files with the priority `build.properties.local > build.properties > build.properties.dist` which means that any
variable set to `build.properties.local` will override both other files.

The `build.properties.local` should contain all overrides for the current system. Production server should also have its
own `build.properties.local` with the server's individual credentials used. Make sure you never commit this file!

All options you can use can be found in the `build.properties.dist` file. Just copy the lines you want to override and
change their values. For example:

```
# Database settings.
drupal.db.name = my_database
drupal.db.user = root
drupal.db.password = hunter2

# Admin user.
drupal.admin.username = admin
drupal.admin.password = admin

# The base URL to use in tests.
drupal.base_url = http://myproject.local

# Verbosity of Drush commands. Set to 'yes' for verbose output.
drush.verbose = yes
```


## Listing the available build commands

You can get a list of all the available Phing build commands ("targets") with a
short description of each target with the following command:

```
$ ./vendor/bin/phing
```

## Install the website.
After you have set up your local database credentials in your `build.properties.local` and any other property run
```
$ ./vendor/bin/phing install
```

## Set up tools for the development environment
If you want to install a version suitable for development you can execute the `setup-dev` Phing target.

```
$ ./vendor/bin/phing setup-dev
```
This will perform the following tasks:

1. Configure PHP CodeSniffer.
2. Enable 'development mode'. This will:
  * Enable the services in `development.services.yml`.
  * Show all error messages with backtrace information.
  * Disable CSS and JS aggregation.
  * Disable the render cache.
  * Allow test modules and themes to be installed.
  * Enable access to `rebuild.php`.
3. Enable development modules.
4. Create a demo user for each user role.

To set up a development environment quickly, you can perform both the `install`
and `setup-dev` targets at once by executing `install-dev`:

```
$ ./vendor/bin/phing install-dev
```

## Running PHPUnit tests

Run the tests from the `web` folder:

```
$ cd web/
$ ../vendor/bin/phpunit
```

By default all tests in the folders `web/modules/custom`, `web/profiles` and
`web/themes/custom` are included when running the tests. Check the section on
PHPUnit in the `build.properties.dist` to customize the tests.


## Checking for coding standards violations

### Set up PHP CodeSniffer

PHP CodeSniffer is included to do coding standards checks of PHP and JS files.
In the default configuration it will scan all files in the following folders:
- `web/modules` (excluding `web/modules/contrib`)
- `web/profiles`
- `web/themes`

First you'll need to execute the `setup-php-codesniffer` Phing target (note that
this also runs as part of the `install-dev` and `setup-dev` targets):

```
$ ./vendor/bin/phing setup-php-codesniffer
```

This will generate a `phpcs.xml` file containing settings specific to your local
environment. Make sure to never commit this file.

### Run coding standards checks

#### Run checks manually

The coding standards checks can then be run as follows:

```
# Scan all files for coding standards violations.
$ ./vendor/bin/phpcs

# Scan only a single folder.
$ ./vendor/bin/phpcs web/modules/custom/mymodule
```

#### Run checks automatically when pushing

To save yourself the embarrassment of pushing non-compliant code to the git
repository you can put the following line in your `build.properties.local`:

```
# Whether or not to run a coding standards check before doing a git push. Note
# that this will abort the push if the coding standards check fails.
phpcs.prepush.enable = 1
```

and then regenerate your PHP CodeSniffer configuration:

```
$ ./vendor/bin/phing setup-php-codesniffer
```

If your project requires all team members to follow coding standards, put this
line in the project configuration (`build.properties`) instead.

Note that this will not allow you to push any code that fails the coding
standards check. If you really need to push in a hurry, then you can disable
the coding standards check by executing this Phing target:

```
$ ./vendor/bin/phing disable-pre-push
```

The pre-push hook will be reinstated when the `setup-php-codesniffer` target
is executed.


### Customize configuration

The basic configuration can be changed by copying the relevant Phing properties
from the "PHP CodeSniffer configuration" section in `build.properties.dist` to
`build.properties` and changing them to your requirements. Then regenerate the
`phpcs.xml` file by running the `setup-php-codesniffer` target:

```
$ ./vendor/bin/phing setup-php-codesniffer
```

To change to PHP CodeSniffer ruleset itself, make a copy of the file
`phpcs-ruleset.xml.dist` and rename it to `phpcs-ruleset.xml`, and then put this
line in your `build.properties` file:

```
phpcs.standard = ${project.basedir}/phpcs-ruleset.xml
```

For more information on configuring the ruleset see [Annotated ruleset](http://pear.php.net/manual/en/package.php.php-codesniffer.annotated-ruleset.php).


## FAQ

### Should I commit the contrib modules I download?

Composer recommends **no**. They provide [argumentation against but also
workrounds if a project decides to do it anyway](https://getcomposer.org/doc/faqs/should-i-commit-the-dependencies-in-my-vendor-directory.md).

### Should I commit the scaffolding files?

The [drupal-scaffold](https://github.com/drupal-composer/drupal-scaffold) plugin can download the scaffold files (like
index.php, update.php, …) to the web/ directory of your project. If you have not customized those files you could choose
to not check them into your version control system (e.g. git). If that is the case for your project it might be
convenient to automatically run the drupal-scaffold plugin after every install or update of your project. You can
achieve that by registering `@composer drupal:scaffold` as post-install and post-update command in your composer.json:

```json
"scripts": {
    "post-install-cmd": [
        "@composer drupal:scaffold",
        "..."
    ],
    "post-update-cmd": [
        "@composer drupal:scaffold",
        "..."
    ]
},
```
### How can I apply patches to downloaded modules?

If you need to apply patches (depending on the project being modified, a pull
request is often a better solution), you can do so with the
[composer-patches](https://github.com/cweagans/composer-patches) plugin.

To add a patch to drupal module foobar insert the patches section in the extra
section of composer.json:
```json
"extra": {
    "patches": {
        "drupal/foobar": {
            "Patch description": "URL or local path to patch"
        }
    }
}
```
### How do I specify a PHP version ?

This project supports PHP 7.3 as minimum version (see [Drupal 9 requirements](https://www.drupal.org/docs/9/how-drupal-9-is-made-and-what-is-included/environment-requirements-of-drupal-9)).

To prevent this you can add this code to specify the PHP version you want to use in the `config` section of `composer.json`:
```json
"config": {
    "sort-packages": true,
    "platform": {
        "php": "7.3.0"
    }
},
```
